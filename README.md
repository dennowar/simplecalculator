**A simple calculator based on Angular2 framework using TypeScript**

## Prerequisites

* install nodejs and npm with your packet manager
* install angular-cli 
```
npm install -g @angular/cli
```
* go to project folder
```
cd SimpleCalculator
```
* install dependencies
```
npm install
```
---

## Run the project

```
ng serve -o
```

---

## Run unit tests

```
ng test
```

---
