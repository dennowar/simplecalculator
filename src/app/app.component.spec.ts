import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {CalcResultComponent} from './calc-result/calc-result.component';
import {CalcButtonsComponent} from './calc-buttons/calc-buttons.component';
import {CalcService} from './calc.service';
describe('AppComponent', () => {

  const calcServiceStub: CalcService = new CalcService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CalcResultComponent,
        CalcButtonsComponent
      ],
      providers:    [ {provide: CalcService, useValue: calcServiceStub } ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Simple Calculator');
  }));
});
