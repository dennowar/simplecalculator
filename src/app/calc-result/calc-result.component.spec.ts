import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcResultComponent } from './calc-result.component';
import {CalcService} from '../calc.service';

describe('CalcResultComponent', () => {
  let component: CalcResultComponent;
  let fixture: ComponentFixture<CalcResultComponent>;

  const calcServiceStub: CalcService = new CalcService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcResultComponent ],
      providers:    [ {provide: CalcService, useValue: calcServiceStub } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

