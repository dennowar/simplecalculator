import { Component, OnInit } from '@angular/core';
import {CalcService} from '../calc.service';

@Component({
  selector: 'app-calc-result',
  templateUrl: './calc-result.component.html',
  styleUrls: ['./calc-result.component.css']
})
export class CalcResultComponent implements OnInit {

  constructor(public calcService: CalcService) { }

  ngOnInit() {
  }

}
