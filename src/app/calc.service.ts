import { Injectable } from '@angular/core';
import {CalculatorFactory} from './calculator/calculators';
import {log} from 'util';
import {CalcNumber} from './model/calc-number';

/**
 * Service which does the heavy stuff
 * Components can use it to manipulate values
 */
@Injectable()
export class CalcService {

  values: Array<string>;
  display: string;
  currentNumber: CalcNumber;
  resetDisplay: boolean;
  lastOperator: string;

  constructor() {
    this.clearAll();
  }

  /**
   * set default values to all fields
   */
  clearAll(): void {
    this.values = [];
    this.display = '0';
    this.currentNumber = new CalcNumber();
    this.resetDisplay = false;
    this.lastOperator = '';
  }

  /**
   * remove the last entry of a number
   */
  clearEntry(): void {
    var entries = this.currentNumber.removeDigit();
    if (entries === '') {
      entries = '0';

    }
    this.display = entries;
  }

  /**
   * add a decimal point if there is none
   */
  addDecimal(): void {
    this.resetDisplayIfNeeded();
    this.display = this.currentNumber.addDecimal();
  }

  /**
   * add a digit to build a number
   * @param {string} digit - add at the end
   */
  addDigit(digit: string): void {
    this.resetDisplayIfNeeded();
    this.display = this.currentNumber.addDigit(digit);
  }

  /**
   * calculation is executed if we have enough information
   * like two values to compute and an operator
   * @param {string} operator computation depends on the operator
   */
  calculate(operator: string): void {
    if (this.currentNumber.isEmpty()){
      return;
    }

    this.values.push(this.currentNumber.value);
    this.currentNumber.reset();

    if (this.values.length === 2) {

      const a = +this.values[0];
      const b = +this.values[1];
      const result  = CalculatorFactory.INSTANCE(this.lastOperator).calculate(a, b);

      log(`${a} ${this.lastOperator} ${b} = ${result}`);

      if (result) {
        this.display = result;
      }

      this.currentNumber.setValue(this.display);
      this.values = [this.currentNumber.value];

      this.resetDisplay = true;

    } else {
      this.resetDisplay = false;
    }

    this.lastOperator = operator;
  }

  /**
   * a wrapper to get the result of a calculation
   */
  calcTotal(): void {
    this.calculate('');
  }

  private resetDisplayIfNeeded(): void {
    if (this.resetDisplay) {
      this.currentNumber.reset();
      this.resetDisplay = false;
    }
  }
}
