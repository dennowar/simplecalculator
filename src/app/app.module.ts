import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CalcResultComponent } from './calc-result/calc-result.component';
import { CalcButtonsComponent } from './calc-buttons/calc-buttons.component';
import {CalcService} from './calc.service';


@NgModule({
  declarations: [
    AppComponent,
    CalcResultComponent,
    CalcButtonsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    CalcService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
