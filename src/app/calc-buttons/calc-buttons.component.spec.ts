import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcButtonsComponent } from './calc-buttons.component';
import {CalcService} from '../calc.service';

describe('CalcButtonsComponent', () => {
  let component: CalcButtonsComponent;
  let fixture: ComponentFixture<CalcButtonsComponent>;

  const calcServiceStub: CalcService = new CalcService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcButtonsComponent ],
      providers:    [ {provide: CalcService, useValue: calcServiceStub } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
