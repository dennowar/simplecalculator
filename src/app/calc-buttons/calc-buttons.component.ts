import { Component, OnInit } from '@angular/core';
import {Button} from '../model/button';
import {ButtonList} from '../model/button-list';
import {CalcService} from '../calc.service';

@Component({
  selector: 'app-calc-buttons',
  templateUrl: './calc-buttons.component.html',
  styleUrls: ['./calc-buttons.component.css']
})
export class CalcButtonsComponent implements OnInit {

  buttons: Button[];

  constructor(public calcService: CalcService) { }

  ngOnInit() {
    this.buttons = new ButtonList(this.calcService).buttons;
  }

}
