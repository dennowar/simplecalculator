import {TestBed, inject, async} from '@angular/core/testing';

import { CalcService } from './calc.service';
import {AppComponent} from './app.component';

describe('CalcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalcService]
    });
  });

  it('should be created', inject([CalcService], (service: CalcService) => {
    expect(service).toBeTruthy();
  }));

  it('clearAll should set defaults', inject([CalcService], (service: CalcService) => {
    expect(service).toBeTruthy();
    service.clearAll();
    expect(service.values.length).toEqual(0);
    expect(service.display).toEqual('0');
    expect(service.resetDisplay).toEqual(false);
    expect(service.lastOperator).toEqual('');
    expect(service.currentNumber).toBeTruthy();

  }));

  it('addDigit should increase numbers value lentgh', inject([CalcService], (service: CalcService) => {
    expect(service.currentNumber.value).toEqual('');
    service.addDigit('1');
    expect(service.currentNumber.value).toEqual('1');
    expect(service.currentNumber.value.length).toEqual(1);
    service.addDigit('1');
    expect(service.currentNumber.value).toEqual('11');
    expect(service.currentNumber.value.length).toEqual(2);

  }));

  it('add Digit should add a point once', inject([CalcService], (service: CalcService) => {
    expect(service.currentNumber.value).toEqual('');

    service.addDigit('1');
    expect(service.currentNumber.value).toEqual('1');
    expect(service.currentNumber.value.length).toEqual(1);

    service.addDecimal();
    service.addDecimal();
    expect(service.currentNumber.value).toEqual('1.');
    expect(service.currentNumber.value.length).toEqual(2);

    service.addDigit('1');
    expect(service.currentNumber.value).toEqual('1.1');
    expect(service.currentNumber.value.length).toEqual(3);
  }));

  it('add Digit should prepend 0', inject([CalcService], (service: CalcService) => {
    expect(service.currentNumber.value).toEqual('');

    service.addDecimal();
    service.addDecimal();
    expect(service.currentNumber.value).toEqual('0.');
    expect(service.currentNumber.value.length).toEqual(2);

    service.addDigit('1');
    expect(service.currentNumber.value).toEqual('0.1');
    expect(service.currentNumber.value.length).toEqual(3);
  }));

  it('clearEntry should remove last digit', inject([CalcService], (service: CalcService) => {
    expect(service.currentNumber.value).toEqual('');

    service.addDigit('1');
    service.addDigit('2');
    service.addDigit('3');
    service.addDigit('4');
    service.addDigit('5');
    expect(service.currentNumber.value).toEqual('12345');
    expect(service.currentNumber.value.length).toEqual(5);

    service.clearEntry();
    expect(service.currentNumber.value).toEqual('1234');
    expect(service.currentNumber.value.length).toEqual(4);

    service.clearEntry();
    service.clearEntry();
    service.clearEntry();
    service.clearEntry();
    expect(service.currentNumber.value).toEqual('');
    expect(service.currentNumber.value.length).toEqual(0);
  }));

  it('calculate needs one value at least', inject([CalcService], (service: CalcService) => {
    expect(service.currentNumber.value).toEqual('');
    service.calculate('');
    expect(service.currentNumber.value).toEqual('');

  }));

  it('calculate has one value', inject([CalcService], (service: CalcService) => {
    service.addDigit('1');
    expect(service.currentNumber.value).toEqual('1');

    service.calculate('+');
    expect(service.values.length).toEqual(1);
    expect(service.currentNumber.value).toEqual('');
    expect(service.resetDisplay).toEqual(false);
    expect(service.lastOperator).toEqual('+');

  }));

  it('calculate has two values', inject([CalcService], (service: CalcService) => {
    service.addDigit('1');
    service.calculate('+');
    service.addDigit('2');
    expect(service.values.length).toEqual(1);
    expect(service.currentNumber.value).toEqual('2');
    expect(service.resetDisplay).toEqual(false);
    expect(service.lastOperator).toEqual('+');

    service.calculate('');
    expect(service.values.length).toEqual(1);
    expect(service.currentNumber.value).toEqual('3');
    expect(service.resetDisplay).toEqual(true);
    expect(service.lastOperator).toEqual('');
  }));

  it('calculate has more values', inject([CalcService], (service: CalcService) => {
    service.addDigit('1');
    service.calculate('+');
    service.addDigit('2');
    service.calculate('');
    service.calculate('-');
    service.addDigit('1');
    service.calculate('');
    expect(service.values.length).toEqual(1);
    expect(service.currentNumber.value).toEqual('2');
    expect(service.resetDisplay).toEqual(true);
    expect(service.lastOperator).toEqual('');
  }));
});
