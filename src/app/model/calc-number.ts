/**
 * A Container which holds the constructed numeric value
 * including floating point
 * The value is restricted to 6 digits
 */
export class CalcNumber {
  private static MAX_DIGITS = 6;

  value = '';
  isDecimal = false;

  constructor() {}

  /**
   * Append a digit the value
   * @param {string} digit
   * @returns {string}
   */
  addDigit(digit: string): string {
    if (this.value.length < CalcNumber.MAX_DIGITS) {
      this.value += digit;
    }
    return this.value;
  }

  /**
   * remove last digit from value
   * @returns {string}
   */
  removeDigit(): string {
    if(this.value.length > 1) {
      this.value = this.value.substring(0, this.value.length - 1);
      if(this.value.length === 1) {
        this.isDecimal = false;
      }
    } else {
      this.reset();
    }
    return this.value;
  }

  /**
   * append a floating point
   * @returns {string}
   */
  addDecimal(): string {
    if (this.isDecimal) {
      return this.value;
    }

    this.isDecimal = true;
    return this.addDigit(this.value === '' ? '0.' : '.');
  }

  /**
   * Check if we are in an initial state
   * @returns {boolean} true if initial, false otherwise
   */
  isEmpty(): boolean {
    return this.value === '' && this.isDecimal === false;
  }

  /**
   * set to the initial state again
   */
  reset(): void {
    this.value = '';
    this.isDecimal = false;
  }

  /**
   * set the value
   * @param {string} value
   */
  setValue(value: string): void {
    this.value = value;
    this.isDecimal = false;
  }
}
