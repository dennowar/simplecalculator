/**
 * A Calculator Button
 * it needs a value to submit,
 * a CSS-class to style the HTML element and
 * an action which is triggered onClick() events
 */
export interface Button {
  value: string;
  class: string;
  action: () => void;
}
