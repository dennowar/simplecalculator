import {Button} from './button';
import {CalcService} from '../calc.service';

/**
 * the configuration of the calculator
 */
export class ButtonList {

  constructor(private calcService: CalcService){}

  public buttons: Button[] = [
    {value: 'AC', class: 'btn btn-outline-warning col-6', action: () => this.calcService.clearAll()},
    {value: 'CE', class: 'btn btn-outline-warning col-6', action: () => this.calcService.clearEntry()},
    {value: '7', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('7')},
    {value: '8', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('8')},
    {value: '9', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('9')},
    {value: '/', class: 'btn btn-outline-primary col-3', action: () => this.calcService.calculate('/')},
    {value: '4', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('4')},
    {value: '5', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('5')},
    {value: '6', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('6')},
    {value: '*', class: 'btn btn-outline-primary col-3', action: () => this.calcService.calculate('*')},
    {value: '1', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('1')},
    {value: '2', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('2')},
    {value: '3', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('3')},
    {value: '-', class: 'btn btn-outline-primary col-3', action: () => this.calcService.calculate('-')},
    {value: ',', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDecimal()},
    {value: '0', class: 'btn btn-outline-secondary col-3', action: () => this.calcService.addDigit('0')},
    {value: '=', class: 'btn btn-outline-success col-3', action: () => this.calcService.calcTotal()},
    {value: '+', class: 'btn btn-outline-primary col-3', action: () => this.calcService.calculate('+')}

  ];
}
