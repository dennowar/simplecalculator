/**
 * Factory provides a calculator
 */
export class CalculatorFactory {

  /**
   * Get a calculator instance
   * @param {string} operator - type of calculation (+,-,*,/)
   * @returns {Calculator} - concrete instance of calculator or a dummy if operator is not valid
   * @constructor
   */
  public static INSTANCE(operator: string): Calculator {
    if (operator === '+') {
      return new SumCalculator();
    } else if (operator === '-') {
      return new SubCalculator();
    } else if (operator === '*') {
      return new ProdCalculator();
    } else if (operator === '/') {
      return new DivCalculator();
    } else {
      return new DummyCalculator();
    }
  }
}

interface Calculator {
  calculate(a: number, b: number): string;
}

class DummyCalculator implements Calculator{
  calculate(a: number, b: number): string {
    return null;
  }

}

class SumCalculator implements Calculator {
  calculate(a: number, b: number): string {
    return (a + b).toString();
  }
}

class SubCalculator implements Calculator {
  calculate(a: number, b: number): string {
    return (a - b).toString();
  }
}

class ProdCalculator implements Calculator {
  calculate(a: number, b: number): string {
    return (a * b).toString();
  }
}

class DivCalculator implements Calculator {
  calculate(a: number, b: number): string {
    console.log(`${a}/${b} = ` + a / b);
    return (a / b).toString();
  }
}
